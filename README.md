# Информация о проекте

jse-06

## Стек технологий

java/Intellij IDEA/GRADLE/Git

## Требования к SOFTWARE

- JDK 1.8

## Команда для сборки проекта

```bash
./gradlew build
```

## Команда для запуска проекта

```bash
java -jar build/libs/task-manager.jar 
# или
./gradlew run
```

## Информация о разработчике

**ФИО**: Баулина Ольга Александровна

**E-MAIL**: golovolomkacom@gmail.com